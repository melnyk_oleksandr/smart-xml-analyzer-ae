package ua.khpi.melnik;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.khpi.melnik.analyzer.XmlAnalyzer;

import java.util.Objects;

/**
 * Class for running application.
 *
 * @author Oleksandr Melnyk
 */
public class Runner {

    private static final Logger LOGGER = LoggerFactory.getLogger(Runner.class);

    /**
     * Entry point of application.
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {

        if (Objects.isNull(args) || args.length < 3) {
            LOGGER.info("Please run the program with the following parameters: " +
                    "<input_origin_file_path> <input_other_sample_file_path> <element_id>");
            return;
        }

        String firstFile = args[0];
        String secondFile = args[1];
        String elementId = args[2];

        XmlAnalyzer.parse(firstFile, secondFile, elementId);
    }
}
