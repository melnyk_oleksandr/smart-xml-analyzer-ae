package ua.khpi.melnik.util;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Utility class which contains methods for working with Jsoup library.
 *
 * @author Oleksandr Melnyk
 */
public class JsoupUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(JsoupUtil.class);
    private static final String CHARSET = "UTF-8";

    /**
     * Find the most similar element in different cases on HTML page.
     *
     * @param elements       map with elements
     * @param primaryElement element to be found
     * @param foundElements  list of found elements on another page
     */
    public static void findElement(Map<Integer, Element> elements, Element primaryElement, List<Element> foundElements) {
        foundElements.forEach(element -> {
            int counter = 0;
            if (!elements.containsValue(element)) {
                if (Objects.nonNull(element.text()) && element.text().equals(primaryElement.text())) {
                    counter++;
                }
                for (Attribute foundAttribute : element.attributes()) {
                    if (primaryElement.hasAttr(foundAttribute.getKey()) &&
                            primaryElement.attr(foundAttribute.getKey()).equals(foundAttribute.getValue())) {
                        counter++;
                    }
                }
                elements.put(counter, element);
            }
        });
    }

    /**
     * Read file with HTML content and find element by id.
     *
     * @param htmlFile        file with HTML content
     * @param targetElementId id of element to be found
     * @return first matching element by ID, starting with this element
     */
    public static Optional<Element> findElementById(File htmlFile, String targetElementId) {
        try {
            Document doc = Jsoup.parse(htmlFile, CHARSET, htmlFile.getAbsolutePath());
            return Optional.of(doc.getElementById(targetElementId));
        } catch (IOException e) {
            LOGGER.error("Error reading [{}] file", htmlFile.getAbsolutePath(), e);
            return Optional.empty();
        }
    }

    /**
     * Read file with HTML content and find elements that match CSS query.
     *
     * @param htmlFile file with HTML content
     * @param cssQuery css-like query
     * @return elements that match the query
     */
    public static Optional<Elements> findElementsByQuery(File htmlFile, String cssQuery) {
        try {
            Document doc = Jsoup.parse(htmlFile, CHARSET, htmlFile.getAbsolutePath());
            return Optional.of(doc.select(cssQuery));
        } catch (IOException e) {
            LOGGER.error("Error reading [{}] file", htmlFile.getAbsolutePath(), e);
            return Optional.empty();
        }
    }

    /**
     * Build css query.
     *
     * @param primaryElement   element to be found
     * @param primaryAttribute element attributes
     * @return built css-like query
     */
    public static String constructCssQuery(Element primaryElement, Attribute primaryAttribute) {
        return primaryElement.tagName() + "[" + primaryAttribute.getKey() + "=\"" + primaryAttribute.getValue() + "\"]";
    }

    /**
     * Build path to element (for example: html > body > div > div[1] > div > a).
     *
     * @param element element to be found
     */
    public static void buildPath(Element element) {
        Elements parentElements = element.parents();
        Collections.reverse(parentElements);
        parentElements.add(element);
        String path = parentElements.stream()
                .map(elem -> elem.tagName() + "[" + elem.elementSiblingIndex() + "]")
                .collect(Collectors.joining(" > "));
        LOGGER.info(path);
    }
}
